package be.skelet.todo.todoserver.todo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static be.skelet.todo.todoserver.todo.TodoController.TODO_BASE_URL;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.CrossOrigin.DEFAULT_ORIGINS;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(TODO_BASE_URL)
public class TodoController {

    static final String TODO_BASE_URL = "todo";

    private TodoRepository todoRepository;

    @Autowired
    public TodoController(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @GetMapping
    public ResponseEntity getListOfTodos() {
        List<Todo> todos = todoRepository.findAll();
        return new ResponseEntity<>(todos, OK);
    }

    @PostMapping()
    public ResponseEntity<Todo> createTodo(@RequestBody Todo todo){
        Todo savedTodo = todoRepository.save(todo);
        return new ResponseEntity<>(savedTodo, CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Todo> flagAsDone(@PathVariable long id){
        Todo todo = todoRepository.getOne(id);
        todo.setDone(true);
        Todo flaggedTodo = todoRepository.save(todo);
        return new ResponseEntity<>(flaggedTodo, OK);
    }


}
