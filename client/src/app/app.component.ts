import { Component, OnInit } from '@angular/core';
import { Todo } from './todo';
import { AppService } from './app.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  todos: Observable<Array<Todo>>;
  newTodo: Todo;

  constructor(private service: AppService) {}

  ngOnInit() {
    this.initNewToDo();
    this.initTodos();
  }

  add(todo: Todo) {
    return this.service
      .add(todo)
      .subscribe(() => this.initTodos());
  }

  finish(todo: Todo) {
    return this.service
      .finish(todo)
      .subscribe(() => this.initTodos());
  }

  mapToIcon(status: boolean) {
    return status ? 'check_box' : 'check_box_outline_blank';
  }

  private initNewToDo() {
    this.newTodo = new Todo('');
  }

  private initTodos() {
    this.todos = this.service.getTodos();
  }
}
