import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Todo } from './todo';
import { Observable } from 'rxjs';
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AppService {

  baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = environment.serverHost;
  }

  getTodos(): Observable<Array<Todo>> {
    return this.http.get<Array<Todo>>(`${this.baseUrl}/todo`);
  }

  add(todo: Todo) {
    return this.http.post(`${this.baseUrl}/todo`, todo);
  }

  finish(todo: Todo) {
    return this.http.put(`${this.baseUrl}/todo/${todo.id}`, '');
  }
}
